import questionary as quest
import socket

Delimit = "\4"
rec_sock_len = 8
#rec_sock_len = 1024
chat_app_port = 8081
IPv4 = socket.AF_INET
TCP = socket.SOCK_STREAM


def get_text(receiving_socket):
    buffer = ""

    socket_open = True
    while socket_open:
        # read any data from the socket
        data = receiving_socket.recv(rec_sock_len)

        # if no data is returned the socket must be closed
        if not data:
            socket_open = False

        # add the data to the buffer
        buffer = buffer + data.decode()

        # is there a terminator in the buffer
        terminator_pos = buffer.find(Delimit)
        # if the value is greater than -1, a \n must exist
        while terminator_pos > -1:
            # get the message from the buffer
            message = buffer[:terminator_pos]
            # remove the message from the buffer
            buffer = buffer[terminator_pos + 1:]
            # yield the message (see below)
            yield message
            # is there another terminator in the buffer
            terminator_pos = buffer.find(Delimit)
            
def send_text(sending_socket, text):
    text = text + Delimit
    data = text.encode()
    sending_socket.send(data)

def SetupServer():
    print("Setting up Chat App in Server mode")
        
    server_socket = socket.socket(IPv4, TCP)
    server_socket.bind(("0.0.0.0", chat_app_port))
    server_socket.listen()
    print("Chat App Server waiting for connection from a Client")

    connection_socket, address = server_socket.accept()
    print("Chat App Server accepted connection from Chat App Client")

    send_text(connection_socket, ("Hello, thanks for connecting to the Chat App Server."+
                                  "\nOnline Chat now in Operation."+
                                  "\nPlease wait for a message from the Server."))

    print("Chat App Server received: ", next(get_text(connection_socket)))

    while True:
        send_text(connection_socket, quest.text("Msg to Client: ", default='OK').ask())

        print("Chat App Client said: ", next(get_text(connection_socket)))
            
    connection_socket.close()
    server_socket.close()

def SetupClient(chat_server_address):
    print("Setting up Chat App in Client mode")

    client_socket = socket.socket(IPv4, TCP)
    client_socket.connect((chat_server_address, chat_app_port))
    print("Chat App Client connected to Chat App Server")

    print("Chat App Server sent: ", next(get_text(client_socket)))

    send_text(client_socket, ("Chat App Client acknowledges connection to the Chat App Server"))

    while True:
        print("Chat App Server said: ", next(get_text(client_socket)))

        send_text(client_socket, quest.text("Msg to Server: ", default='OK').ask())

    client_socket.close()

app_mode = quest.select(
    "FL Chat App: Run as Client or Server",
    choices = [
        "Server",
        "Client"
    ]).ask()

if app_mode == "Server":
    SetupServer()
else:
    SetupClient(quest.text("Chat App Server address ", default='localhost').ask() )
