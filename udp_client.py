# 2.4 DJR UDP client
import socket

udp_client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server_address = ("localhost", 20001)
message = "Message from DJR udp client"

udp_client.sendto(message.encode(), server_address)

data, response_address = udp_client.recvfrom(1024)
print ("Ack from Server at ", response_address, " was - [ ", data.decode(), " ]")
